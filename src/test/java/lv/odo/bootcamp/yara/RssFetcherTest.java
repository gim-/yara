package lv.odo.bootcamp.yara;

import java.util.Date;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lv.odo.bootcamp.yara.entities.Feed;
public class RssFetcherTest {
	// TODO finish rss fetcher test
	public static Feed feed;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		feed = new Feed();
		feed.setFeedUrl("http://feeds.tvnet.lv/tvnet/all/latest");
		feed.setLastUpdated(new Date(0));
	}

	@Test
	public void testRssFetcher() {
		RssFetcher rf = new RssFetcher();
		Assert.assertNotNull(rf);
	}
	
	@Test
	public void testFetchEntries() {
		int countBefore = EntityManager.getFeedList().size();
		RssFetcher.fetchEntries(feed);
		int countAfter = EntityManager.getFeedList().size();
		Assert.assertNotEquals(countBefore, countAfter);
		EntityManager.deleteFeed(feed.getId());
	}
}
