package lv.odo.bootcamp.yara;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;

/**
 * Class directly interacts with a database in order to get and set feeds,
 * entries.
 * 
 * @author student
 *
 */
public final class EntityManager {
	private static Logger logger = Logger.getLogger(EntityManager.class.getName());

	/**
	 * if session still has not been open then opens it. tries to save parsed
	 * feed.
	 * 
	 * @param feed
	 *            feed to be saved.
	 * @return result of transaction.
	 */
	public static boolean saveFeed(Feed feed) {
		Session session = HibernateUtil.getSession();
		try {
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(feed);
			transaction.commit();
		} catch (Exception e) {
			logger.error("saveFeed ERROR: " + e);
			return false;
		}
		return true;
	}

	/**
	 * Gets feed from a database.
	 * 
	 * @param id
	 *            identicifator indicating feed to be red.
	 * @return returns the feed.
	 */
	public static Feed getFeed(int id) {
		Session session = HibernateUtil.getSession();
		Feed result = (Feed) session.get(Feed.class, id);
		return result;
	}

	/**
	 * Deletes a feed via opened session and using parsed identificator.
	 * 
	 * @param id
	 *            identificator to be used in order to delete selected feed.
	 */
	public static void deleteFeed(int id) {
		Feed feed = new Feed();
		feed.setTitle("");
		feed.setId(id);

		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		session.createQuery("delete yara_feed_entries where feed_id=" + id).executeUpdate();
		session.createQuery("delete yara_feeds where id=" + id).executeUpdate();
		// session.delete(feed); // ?!
		transaction.commit();
	}

	/**
	 * Creates list of feed queries
	 * 
	 * @return returns the list of queries.
	 */
	@SuppressWarnings("unchecked")
	public static List<Feed> getFeedList() {
		Session session = HibernateUtil.getSession();
		Query q = session.createQuery("from yara_feeds");
		return q.list();
	}

	/**
	 * Saves feed entry parsed as a value.
	 * 
	 * @param entry
	 *            feed entry to be saved as an entry.
	 * @return returned is the progress of transaction.
	 */
	public static boolean saveFeedEntry(FeedEntry entry) {
		Session session = HibernateUtil.getSession();
		try {
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(entry);
			transaction.commit();
		} catch (Exception e) {
			logger.error("saveFeedEntry ERROR: " + e);
			return false;
		}
		return true;
	}

	/**
	 * Gets entry of a feed, via opened session, by using id.
	 * 
	 * @param id
	 *            parameter for entry to be recognized by.
	 * @return returns red entry.
	 */
	public static FeedEntry getFeedEntry(int id) {
		FeedEntry result = null;
		Session session = HibernateUtil.getSession();
		result = (FeedEntry) session.get(FeedEntry.class, id);
		return result;
	}

	/**
	 * populates list of queries. @fixme Replace with something better than a
	 * plane SQL statement.
	 * 
	 * @return a list of queries required to get entries.
	 */
	public static long getFeedUnreadCount(Feed feed) {
		Session session = HibernateUtil.getSession();
		long count = 0;
		Query query = session.createQuery(
				"SELECT COUNT(id) FROM yara_feed_entries WHERE feed_id=" + feed.getId() + " AND isRead IS FALSE");
		count = (long) query.uniqueResult();

		return count;
	}

	/**
	 * Returns a List object containing all specific feed entries
	 * 
	 * @param feed
	 *            A feed to get entries for
	 * @return List with feed entries
	 */
	@SuppressWarnings("unchecked")
	public static List<FeedEntry> getFeedEntryList(Feed feed) {
		Session session = HibernateUtil.getSession();
		Query q = session.createQuery("from yara_feed_entries where feed_id=" + feed.getId());
		return q.list();
	}
}
