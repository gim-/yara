package lv.odo.bootcamp.yara.gui;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import lv.odo.bootcamp.yara.EntityManager;
import lv.odo.bootcamp.yara.entities.Feed;
import lv.odo.bootcamp.yara.entities.FeedEntry;

public class MainWindow extends JFrame {
	private static Logger logger = Logger.getLogger(MainWindow.class.getName());
	private static final long serialVersionUID = 1197277186947073597L;
	private final static String APP_LOOK_FEEL = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";

	private JPanel contentPane;
	private JTable tableEntries;
	public JTree treeFeeds;
	private JTextPane textContent;
	private JButton btnAddFeed;
	private JButton btnRemoveFeed;
	private JButton btnRefreshFeeds;

	private ArrayList<Integer> entryIdArray;
	public ArrayList<Integer> feedIdArray;

	private JPopupMenu contextMenuFeed;
	private JPopupMenu contextMenuFeedList;
	private JButton btnRefreshFeed;
	private JEditorPane epEntryLink;
	private JButton btnMarkAllAs;

	private boolean refreshCase = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					logger.error("main ERROR: " + e);
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creates main GUI frame containing menus, tables and buttons to operate
	 * the program.
	 */
	public MainWindow() {
		setTitle("YARA");
		setLookAndFeel(APP_LOOK_FEEL);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setMinimumSize(new Dimension(800, 600));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(toolBar);

		btnAddFeed = new JButton("+ Add Feed");
		toolBar.add(btnAddFeed);

		btnRemoveFeed = new JButton("- Remove Feed");
		toolBar.add(btnRemoveFeed);

		btnRefreshFeed = new JButton("Refresh Feed");
		toolBar.add(btnRefreshFeed);

		btnRefreshFeeds = new JButton("Refresh All Feeds");
		btnRefreshFeeds.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		toolBar.add(btnRefreshFeeds);

		btnMarkAllAs = new JButton("Mark All As Read");
		btnMarkAllAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		toolBar.add(btnMarkAllAs);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.1);
		contentPane.add(splitPane);

		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.5);

		splitPane_1.setPreferredSize(new Dimension(150, 100));
		splitPane_1.setMinimumSize(new Dimension(100, 50));
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setRightComponent(splitPane_1);

		JScrollPane scrollEntries = new JScrollPane((Component) null);
		splitPane_1.setLeftComponent(scrollEntries);

		tableEntries = new JTable() {
			private static final long serialVersionUID = 1976211033179636035L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// Disallow to edit cells
				return false;
			};
		};
		tableEntries.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollEntries.setViewportView(tableEntries);
		tableEntries.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableEntries.getTableHeader().setReorderingAllowed(false);

		JScrollPane scrollContent = new JScrollPane((Component) null, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		splitPane_1.setRightComponent(scrollContent);

		textContent = new JTextPane();
		textContent.setPreferredSize(new Dimension(100, 200));
		textContent.setMinimumSize(new Dimension(100, 100));
		textContent.setEditable(false);
		textContent.setContentType("text/html");
		textContent.setAutoscrolls(true);
		scrollContent.setViewportView(textContent);

		epEntryLink = new JEditorPane();
		epEntryLink.setFont(new Font("Dialog", Font.BOLD, 12));
		epEntryLink.setEditable(false);
		epEntryLink.setContentType("text/html");
		scrollContent.setColumnHeaderView(epEntryLink);
		JScrollPane scrollFeeds = new JScrollPane();
		scrollFeeds.setPreferredSize(new Dimension(100, Integer.MAX_VALUE));
		splitPane.setLeftComponent(scrollFeeds);

		treeFeeds = new JTree();
		scrollFeeds.setViewportView(treeFeeds);
		treeFeeds.setCellRenderer(new FeedTreeRenderer());

		addEventListeners();
		createContextMenus();
		loadFeeds();
	}

	/**
	 * sets the design of the program.
	 * 
	 * @param className
	 *            class of the new design.
	 */
	private void setLookAndFeel(String className) {
		try {
			UIManager.setLookAndFeel(className);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			System.out.println(e.getMessage());
			logger.error("setLookAndFeel ERROR: " + e);
		}
	}

	/**
	 * adds event listeners to lists.
	 */
	private void addEventListeners() {
		btnMarkAllAs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int selectedRow = treeFeeds.getMinSelectionRow();
				loadFeeds();
				treeFeeds.setSelectionRow(selectedRow);
			}
		});

		btnRefreshFeeds.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int selectedRow = treeFeeds.getMinSelectionRow();
				loadFeeds();
				treeFeeds.setSelectionRow(selectedRow);
			}
		});

		btnRefreshFeed.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int selectedRow = treeFeeds.getMinSelectionRow();
				loadFeeds();
				treeFeeds.setSelectionRow(selectedRow);
			}
		});

		treeFeeds.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					TreePath path = treeFeeds.getPathForLocation(e.getX(), e.getY());
					Rectangle pathBounds = treeFeeds.getUI().getPathBounds(treeFeeds, path);
					if (pathBounds != null && pathBounds.contains(e.getX(), e.getY())) {
						treeFeeds.setSelectionPath(path);
						contextMenuFeed.show(treeFeeds, pathBounds.x, pathBounds.y + pathBounds.height);
					} else {
						contextMenuFeedList.show(treeFeeds, e.getX(), e.getY());
					}
				}
			}
		});

		treeFeeds.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				loadEntries();
			}
		});

		tableEntries.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting())
					return;
				int index = tableEntries.getSelectedRow();
				if (index == -1)
					return;
				int id = entryIdArray.get(index);
				loadContent(id);
				FeedEntry entry = EntityManager.getFeedEntry(id);
				tableEntries.setValueAt(entry.getPublicationDate().toString(), index, 0);
				tableEntries.setValueAt(entry.getTitle(), index, 1);
				epEntryLink.setText("<html><a href=\"" + entry.getLink() + "\">" + entry.getTitle() + "</a></html>");
				epEntryLink.setToolTipText(entry.getLink());
				entry.setRead(true);
				EntityManager.saveFeedEntry(entry);
				if (refreshCase)
					updateUnreadCount();
				refreshCase = true;
			}
		});

		HyperlinkListener hyperLinkListener = new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (hle.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
					Desktop desktop = Desktop.getDesktop();
					try {
						desktop.browse(hle.getURL().toURI());
					} catch (Exception ex) {
						logger.error("hyperlinkUpdate ERROR: " + ex);
						ex.printStackTrace();
					}
				}
			}
		};
		textContent.addHyperlinkListener(hyperLinkListener);
		epEntryLink.addHyperlinkListener(hyperLinkListener);

		btnAddFeed.setActionCommand(FeedActionListener.ACTION_ADD_FEED);
		btnAddFeed.addActionListener(new FeedActionListener(this));

		btnRemoveFeed.setActionCommand(FeedActionListener.ACTION_REMOVE_FEED);
		btnRemoveFeed.addActionListener(new FeedActionListener(this));

		btnRefreshFeed.setActionCommand(FeedActionListener.ACTION_REFRESH_FEED);
		btnRefreshFeed.addActionListener(new FeedActionListener(this));

		btnRefreshFeeds.setActionCommand(FeedActionListener.ACTION_REFRESH_ALL_FEEDS);
		btnRefreshFeeds.addActionListener(new FeedActionListener(this));

		btnMarkAllAs.setActionCommand(FeedActionListener.ACTION_MARK_ALL_READ);
		btnMarkAllAs.addActionListener(new FeedActionListener(this));
	}

	private void populateMenu(JPopupMenu contextMenu, JMenuItem[] items) {
		for (JMenuItem item : items) {
			contextMenu.add(item);
		}
	}

	/**
	 * add content to the menu.
	 * 
	 * @param contextMenu
	 *            menu to be populated.
	 * @param items
	 *            items for menu to be populated with.
	 */
	private void createContextMenus() {
		// FIXME JMenu
		JMenuItem newMenuItem = new JMenuItem("Add Feed");
		JMenuItem deleteMenuItem = new JMenuItem("Delete Feed");
		JMenuItem renameMenuItem = new JMenuItem("Rename Feed");
		contextMenuFeed = new JPopupMenu();
		contextMenuFeedList = new JPopupMenu();
		newMenuItem.setActionCommand(FeedActionListener.ACTION_ADD_FEED);
		newMenuItem.addActionListener(new FeedActionListener(this));
		deleteMenuItem.setActionCommand(FeedActionListener.ACTION_REMOVE_FEED);
		deleteMenuItem.addActionListener(new FeedActionListener(this));
		renameMenuItem.setActionCommand(FeedActionListener.ACTION_RENAME_FEED);
		renameMenuItem.addActionListener(new FeedActionListener(this));

		JMenuItem[] contextMenu = new JMenuItem[] { newMenuItem, deleteMenuItem, renameMenuItem };

		populateMenu(contextMenuFeed, contextMenu);

		JMenuItem newMenuItem2 = new JMenuItem("Add Feed");
		newMenuItem2.setActionCommand(FeedActionListener.ACTION_ADD_FEED);
		newMenuItem2.addActionListener(new FeedActionListener(this));
		JMenuItem[] contextMenuList = new JMenuItem[] { newMenuItem2 };
		populateMenu(contextMenuFeedList, contextMenuList);
	}

	/**
	 * Creates menu from feedList
	 */
	public void loadFeeds() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Feeds");
		DefaultTreeModel model = new DefaultTreeModel(root);
		List<Feed> list = EntityManager.getFeedList();
		feedIdArray = new ArrayList<>();
		for (Feed b : list) {
			DefaultMutableTreeNode feedNode = new DefaultMutableTreeNode(b);
			root.add(feedNode);
			feedIdArray.add(b.getId());
		}

		treeFeeds.setModel(model);
	}

	/**
	 * Loads titles and dates in a table.
	 */
	public void loadEntries() {
		String[] columnNames = { "Date", "Title" };
		String[] rowData = new String[2];
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeFeeds.getLastSelectedPathComponent();
		if (node == null)
			return;
		if (node.isLeaf()) {
			int selectedIndex = treeFeeds.getMinSelectionRow();
			Feed feed = new Feed();
			feed.setId(feedIdArray.get(selectedIndex - 1));
			List<FeedEntry> entryList = EntityManager.getFeedEntryList(feed);
			Collections.sort(entryList, new Comparator<FeedEntry>() {
				@Override
				public int compare(FeedEntry o1, FeedEntry o2) {
					return o2.getPublicationDate().compareTo(o1.getPublicationDate());
				}
			});
			DefaultTableModel tableModel = new DefaultTableModel();
			tableModel.addColumn(columnNames[0]);
			tableModel.addColumn(columnNames[1]);

			entryIdArray = new ArrayList<>();
			int counter = 0;
			for (FeedEntry entry : entryList) {
				rowData[0] = entry.isRead() ? entry.getPublicationDate().toString()
						: "<html><b>" + entry.getPublicationDate().toString() + "</b></html>";
				rowData[1] = entry.isRead() ? entry.getTitle() : "<html><b>" + entry.getTitle() + "</b></html>";
				tableModel.insertRow(counter, rowData);
				entryIdArray.add(entry.getId());
				counter++;
			}
			tableEntries.setModel(tableModel);
			tableEntries.getColumnModel().getColumn(0).setPreferredWidth(180);
			tableEntries.getColumnModel().getColumn(0).setMaxWidth(180);
		}
	}

	/**
	 * gets content from a feed.
	 * 
	 * @param id
	 *            specifies which feeds content should be red.
	 */

	private void loadContent(int id) {
		FeedEntry entry = EntityManager.getFeedEntry(id);
		String html = entry.getContent();
		Document doc = Jsoup.parse(html);
		Element image = doc.select("img").first();
		if (image != null) {
			BufferedImage bimg = null;
			try {
				bimg = ImageIO.read(new URL(image.attr("abs:src")));
				int width = bimg.getWidth();
				int height = bimg.getHeight();
				while (width > contentPane.getSize().getWidth() / 1.5) {
					width /= 2;
					height /= 2;
				}
				image.attr("height", height + "");
				image.attr("width", width + "");
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		String text = doc.body().html();
		// String url = link.absUrl("src");

		textContent.setText(text);
		textContent.setCaretPosition(0);
	}

	private void updateUnreadCount() {
		int selectedRow = treeFeeds.getMinSelectionRow();
		int selectedTableRow = tableEntries.getSelectedRow();
		refreshCase = false;
		loadFeeds();
		treeFeeds.setSelectionRow(selectedRow);
		tableEntries.setRowSelectionInterval(selectedTableRow, selectedTableRow);
	}
}
